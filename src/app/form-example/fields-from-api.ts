
import { FormFactoryModel } from '../form-factory/core/models/form-factory';
import { SectionFactoryModel } from '../form-factory/core/models/section-factory';

export const sectionFromApi: SectionFactoryModel[] = [
  {
    nameSection: "Datos Personales",
    sourceDataSave: "string",
    controlType: 'section',
    formGroupName: 'section-0',
    colSize: 'col-12 sm:col-12',
    rightIcon:"pi pi-user",
    forms: [
      {
        colSize: 'col-12 sm:col-12',
        group: [
          {
            controlType: 'checkbox',
            options: {
              label: 'Titulo',
              formControlName: 'activeTitle',
              value: true,
              childrenActions: [
                {
                  childControl: "title",
                  actions: [{
                    parentAction: "activate",
                    childAction: "visible",
                    reset: true
                  }]
                }
              ]
            }
          },
          {
            controlType: 'input',
            options: {
              type: 'text',
              containerClass: 'mb-0',
              label: 'Título',
              placeholder: 'Título universitario',
              formControlName: 'title',
              value: '',
              disabled: false,
              validators: [{
                type: "required",
                errorMsj: "El campo título es requerido"
              }, {
                type: "maxLength",
                value: 12,
                errorMsj: "No debe pasar de 12 caracteres"
              },
              {
                type: "minLength",
                value: 2,
                errorMsj: "No debe ser menor a 2"
              },
              {
                type: "email",
                errorMsj: "El email no tiene el formato correcto"
              }
              ]
            },
          },
          {
            controlType: 'input',
            options: {
              type: 'text',
              containerClass: 'mb-0',
              label: 'Subtitle',
              placeholder: '',
              formControlName: 'subtitle',
              value: '',
              disabled: false,
              validators: [{
                type: "required",
                value: true,
                errorMsj: "El campo subtitulo es requerido"
              }, {
                type: "maxLength",
                value: 200,
                errorMsj: "No debe pasar de &"
              }],
            },
          },
          {
            controlType: 'input',
            options: {
              type: 'text',
              containerClass: 'mb-0',
              label: 'Descripción',
              placeholder: '',
              formControlName: 'descripcion',
              value: '',
              disabled: false,
              validators: [{
                type: "required",
                value: true,
                errorMsj: "El campo descripción es requerido"
              }, {
                type: "maxLength",
                value: 20,
                errorMsj: "El valor no puede sobrepasar de 20"
              }],
            },
          },
        ],
      },
    ]
  },{
    nameSection: "Descripción",
    //formControlName: string;
    sourceDataSave: "string",
    controlType: 'section',
    formGroupName: 'section-1',
    colSize: 'col-12 sm:col-10',
    forms: [
      {
        colSize: 'col-12 sm:col-5',
        group: [
          {
            controlType: 'checkbox',
            options: {
              label: 'Titulo',
              formControlName: 'activeTitle2',
              value: true,
              childrenActions: [
                {
                  childControl: "title2",
                  actions: [{
                    parentAction: "activate",
                    childAction: "visible",
                    reset: true
                  }]
                }
              ]
            }
          },
          {
            controlType: 'input',
            options: {
              type: 'text',
              containerClass: 'mb-0',
              label: 'Title',
              placeholder: 'Título universitario',
              formControlName: 'title2',
              value: '',
              disabled: false,
              validators: [{
                type: "required",
                errorMsj: "El campo titulo es requerido"
              }, {
                type: "maxLength",
                value: 5,
                errorMsj: "No debe pasar de 5 caracteres"
              },
              {
                type: "minLength",
                value: 2,
                errorMsj: "No debe ser menor a 2"
              },
              {
                type: "email",
                errorMsj: "El email no tiene el formato correcto"
              }
              ]
            },
          },
          {
            controlType: 'input',
            options: {
              type: 'text',
              containerClass: 'mb-0',
              label: 'Subtitle',
              placeholder: '',
              formControlName: 'subtitle2',
              value: '',
              disabled: false,
              validators: [{
                type: "required",
                value: true,
                errorMsj: "El campo subtitulo es requerido"
              }, {
                type: "maxLength",
                value: 200,
                errorMsj: "No debe pasar de &"
              }],
            },
          },
          {
            controlType: 'input',
            options: {
              type: 'text',
              containerClass: 'mb-0',
              label: 'Descripción',
              placeholder: '',
              formControlName: 'descripcion2',
              value: '',
              disabled: false,
              validators: [{
                type: "required",
                value: true,
                errorMsj: "El campo descripción es requerido"
              }, {
                type: "maxLength",
                value: 20,
                errorMsj: "El valor no puede sobrepasar de 20"
              }],
            },
          },
        ],
      },{
        controlType: 'input',
        colSize: 'col-12 sm:col-5',
        options: {
          type: 'text',
          label: 'Description',
          formControlName: 'description',
          value: '',
          rows: 5,
          validators: [{
            type: "required",
            errorMsj: "El campo descripción es requerido"
          }, {
            type: "maxLength",
            value: 10,
            errorMsj: "El valor no puede sobrepasar de 10"
          }],
        },
      },
    ]
  },
  {
    nameSection: "Sección 3",
    //formControlName: string;
    sourceDataSave: "string",
    controlType: 'section',
    formGroupName: 'section-2',
    colSize: 'col-12 sm:col-5',
    forms: [
      {
        colSize: 'col-12 sm:col-5',
        group: [
          {
            controlType: 'checkbox',
            options: {
              label: 'Titulo',
              formControlName: 'activeTitle',
              value: true,
              childrenActions: [
                {
                  childControl: "title",
                  actions: [{
                    parentAction: "activate",
                    childAction: "visible",
                    reset: true
                  }]
                }
              ]
            }
          },
          {
            controlType: 'input',
            options: {
              type: 'text',
              containerClass: 'mb-0',
              label: 'Title',
              placeholder: 'Título universitario',
              formControlName: 'title',
              value: '',
              disabled: false,
              validators: [{
                type: "required",
                errorMsj: "El campo titulo es requerido"
              }, {
                type: "maxLength",
                value: 5,
                errorMsj: "No debe pasar de 5 caracteres"
              },
              {
                type: "minLength",
                value: 2,
                errorMsj: "No debe ser menor a 2"
              },
              {
                type: "email",
                errorMsj: "El email no tiene el formato correcto"
              }
              ]
            },
          },
          {
            controlType: 'input',
            options: {
              type: 'text',
              containerClass: 'mb-0',
              label: 'Subtitle',
              placeholder: '',
              formControlName: 'subtitle',
              value: '',
              disabled: false,
              validators: [{
                type: "required",
                value: true,
                errorMsj: "El campo subtitulo es requerido"
              }, {
                type: "maxLength",
                value: 200,
                errorMsj: "No debe pasar de &"
              }],
            },
          },
          {
            controlType: 'input',
            options: {
              type: 'text',
              containerClass: 'mb-0',
              label: 'Descripción',
              placeholder: '',
              formControlName: 'descripcion',
              value: '',
              disabled: false,
              validators: [{
                type: "required",
                value: true,
                errorMsj: "El campo descripción es requerido"
              }, {
                type: "maxLength",
                value: 20,
                errorMsj: "El valor no puede sobrepasar de 20"
              }],
            },
          },
        ],
      }
    ]
  }
] 


export const fieldsFromApi: FormFactoryModel[] = [  
  {
    colSize: 'col-12 sm:col-3',
    group: [
      {
        controlType: 'checkbox',
        options: {
          label: 'Titulo',
          formControlName: 'activeTitle',
          value: true,
          childrenActions: [
            {
              childControl: "title",
              actions: [{
                parentAction: "activate",
                childAction: "visible",
                reset: true
              }]
            }
          ]
        }
      },
      {
        controlType: 'input',
        options: {
          type: 'text',
          containerClass: 'mb-0',
          label: 'Title',
          placeholder: 'Título universitario',
          formControlName: 'title',
          value: '',
          disabled: false,
          validators: [{
            type: "required",
            errorMsj: "El campo titulo es requerido"
          }, {
            type: "maxLength",
            value: 5,
            errorMsj: "No debe pasar de 5 caracteres"
          },
          {
            type: "minLength",
            value: 2,
            errorMsj: "No debe ser menor a 2"
          },
          {
            type: "email",
            errorMsj: "El email no tiene el formato correcto"
          }
          ]
        },
      },
      {
        controlType: 'input',
        options: {
          type: 'text',
          containerClass: 'mb-0',
          label: 'Subtitle',
          placeholder: '',
          formControlName: 'subtitle',
          value: '',
          disabled: false,
          validators: [{
            type: "required",
            value: true,
            errorMsj: "El campo subtitulo es requerido"
          }, {
            type: "maxLength",
            value: 200,
            errorMsj: "No debe pasar de &"
          }],
        },
      },
      {
        controlType: 'input',
        options: {
          type: 'text',
          containerClass: 'mb-0',
          label: 'Descripción',
          placeholder: '',
          formControlName: 'descripcion',
          value: '',
          disabled: false,
          validators: [{
            type: "required",
            value: true,
            errorMsj: "El campo descripción es requerido"
          }, {
            type: "maxLength",
            value: 20,
            errorMsj: "El valor no puede sobrepasar de 20"
          }],
        },
      },
    ],
  },
  {
    controlType: 'textarea',
    colSize: 'col-12 sm:col-3',
    options: {
      type: 'text',
      label: 'Description',
      formControlName: 'description',
      value: '',
      rows: 5,
      validators: [{
        type: "required",
        errorMsj: "El campo descripción es requerido"
      }, {
        type: "maxLength",
        value: 10,
        errorMsj: "El valor no puede sobrepasar de 10"
      }],
    },
  },
  {
    colSize: 'col-12 sm:col-3',
    group: [
      {
        controlType: 'dropdown',
        options: {
          label: 'Lista',
          placeholder: 'Chose',
          formControlName: 'listOptions',
          optionValue: 'value',
          optionLabel: 'label',
          dropdownOptions: [
            {
              label: 'Item 1',
              value: 1,
            }, {
              label: 'Item 2',
              value: 2,
            },
          ],
          value: [],
          validators: [{
            type: "required",
            errorMsj: "Debe seleccionar una opción"
          }],
        },
      },
      {
        controlType: 'checkbox',
        options: {
          label: 'Remember me',
          formControlName: 'remember',
          value: true,
          validators: [{
            type: "required",
            errorMsj: "Debe seleccionar la casilla"
          }],
        },
      },
    ],
  },
  {
    colSize: 'col-12 sm:col-3',
    group: [
      {
        controlType: 'dropdown',
        options: {
          label: 'Lista',
          placeholder: 'Chose',
          formControlName: 'listOptions2',
          optionValue: 'value',
          optionLabel: 'label',
          dropdownOptions: [
            {
              label: 'Item 1',
              value: 1,
            }, {
              label: 'Item 2',
              value: 2,
            },
          ],
          value: [],
          validators: [{
            type: "required",
            errorMsj: "Debe seleccionar una opción"
          }],
        },
      },
      {
        controlType: 'checkbox',
        options: {
          label: 'Remember me',
          formControlName: 'remember2',
          value: true,
          validators: [{
            type: "required",
            errorMsj: "Debe seleccionar la casilla"
          }],
        },
      },
    ],
  },
  {
    dummyFields: [
      {
        options: {
          formControlName: 'dummyField',
          value: 'Hello!',
        },
      },
    ],
  },
];
