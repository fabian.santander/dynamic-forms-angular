import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { FormFactoryService } from '../form-factory/core/services/form-factory.service';
import { FormFactoryModel } from '../form-factory/core/models/form-factory';
import { fieldsFromApi, sectionFromApi } from './fields-from-api';
import { SectionFactoryModel } from '../form-factory/core/models/section-factory';
import { FormFunctions } from '../form-factory/common/utils';

@Component({
  selector: 'app-form-example',
  template: `
     <section>
      <p-button
        styleClass="mb-3"
        label="Submit"
        (click)="onSubmit()"
      ></p-button>
      <p-button
        styleClass="mb-3 ml-2 p-button-secondary"
        label="Reset"
        (click)="onReset()"
      ></p-button>
    </section> 
    <!-- EXAMPLE 1: -->
    <!-- <app-form-factory
      [form]="exampleForm"
      [fields]="exampleFields"
    ></app-form-factory> -->

    <!-- EXAMPLE 2: -->
    <!-- <app-form-factory
      [form]="formFactory.getFormGroup(exampleForm, 'dynamic')"
      [fields]="exampleFields"
    ></app-form-factory>
    <pre>{{ formOutput | json }}</pre> -->
    
    <app-section-factory
      [form]="formSections"
      [sections]="sections"
    ></app-section-factory>
    <!-- <pre>{{ formOutputSection | json }}</pre> -->
  `,
})
export class FormExampleComponent implements OnInit {
  exampleForm!: FormGroup;
  formSections!: FormGroup;
  formOutput: any;
  backValuesFormSections: any
  formOutputSection:any;
  exampleFields: FormFactoryModel[] = fieldsFromApi;
  sections: SectionFactoryModel[] = sectionFromApi;

  constructor(
    private fb: FormBuilder,
    public formFactory: FormFactoryService
  ) {}

  ngOnInit(): void {
    /**
     * EXAMPLE 1:
     *
     * Call formFactory service and pass form fields JSON configuration
     * to create new form group
     */
    // this.exampleForm = this.formFactory.createForm(this.exampleFields);
    /**
     * Example how to add additional formControl to form if you need
     */
    // this.exampleForm.addControl('id', new FormControl(1, Validators.required));

    /**
     * EXAMPLE 2:
     *
     * You can use this approach too to create forms if you also want to use
     * native angular form builder, but in this situation make sure to pass
     * 'dynamic' form group to app-form-factory component instead exampleForm
     */
    this.exampleForm = this.fb.group({
      dynamic: this.formFactory.createForm(this.exampleFields),
      id: [1],
    });

    this.formSections = this.formFactory.createSection(this.sections);
    
    this.formOutput = this.exampleForm.getRawValue();
    this.formOutputSection = this.formSections.getRawValue();
    this.backValuesFormSections = this.formSections.getRawValue();
  }

  onSubmit() {
    FormFunctions.touchAllFormFields(this.formSections);
   // this.formOutput = this.exampleForm.getRawValue();
    this.formOutputSection = this.formSections.getRawValue();
    FormFunctions.getErrorsFromGroup(this.formSections, this.sections);
  }

  onReset() {
    this.formOutput = this.formSections.getRawValue();
    this.formSections.reset();
    this.formSections.patchValue(this.backValuesFormSections);
  }
  
}
