export enum EnumTipoControl {
  checkbox = 'checkbox',
  input = 'input',
  textarea = 'textarea',
  dropdown = 'dropdown',
  section = 'section'
}