export enum EnumTipoValidacion {
  required = 'required',
  maxLength = 'maxLength',
  minLength = 'minLength',
  min = 'min',
  email = 'email'
}