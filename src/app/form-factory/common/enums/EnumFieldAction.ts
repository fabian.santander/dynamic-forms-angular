export enum EnumFieldAction {
  activate ='activate',
  visible = 'visible', 
  enabled = 'enabled',
  change = 'change'
}