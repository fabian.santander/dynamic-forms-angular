import { FormArray, FormControl, FormGroup } from "@angular/forms";
import { ValidatorField } from "src/app/form-factory/core/models/validator-field";

export class FormFunctions {

    /**
     * @description Función que permite obtener los errores que se presentan en el controlador para asignar los mensajes correspondientes a los elementos
     * @author Fabián Santander
     * @date 2023-01-18
     * @param {any} control:any
     * @param {any} optionsElement:any
     * @returns {any}
     */
    static getErrors(control: any, validators: any[]): ValidatorField[] {
        let errorsCtrl = control.errors
        let errorsResult: ValidatorField[] = [];
        if (errorsCtrl)
            errorsResult = validators.filter((validator: ValidatorField) => Object.keys(errorsCtrl).find(val => val.toLowerCase() == validator.type.toLowerCase()));
        return errorsResult;
    }

    /**
     * Función que permite obtener la lista de errores de un FormGroup (Sección o Secciones)
     * @author Fabián Santander
     * @date 2023-01-25
     * @param {any} formGroup:FormGroup
     * @param {any} fields:any
     * @returns {any}
     */
    static getErrorsFromGroup(formGroup: FormGroup, fields: any) {
        let resErrors: any[] = [];

        let getOptionsFields = (lstFields) => {//funcion que permite obtener las opciones de los campos de una sección
            let options: any[] = [];
            lstFields.forEach((field: any) => {
                // Compruebe si este es un grupo de campos
                if (field.group) {                  
                    field.group.forEach((singleField: any) => {
                        options.push(singleField.options);
                    });
                } else {
                    options.push(field.options);
                }
            });
            return options;
        }

        let getAllErrors = (form, filedsForm: any[],) => {//obtiene la lista de errores de una sección
            let errors: any = [];
            const options = getOptionsFields(filedsForm); //otiene las opciones correspondiente a los campos de la sección
            Object.keys(form.controls).forEach(fieldCtrl => {//realiza un barrrido de los campos de la sección
                let control = form.controls[fieldCtrl];//obtiene el controlador de la sección
                let oprtionsCtrl = options.find(f => f.formControlName == fieldCtrl); //obtiene las opciones del controlador
                let resErrs = this.getErrors(control, oprtionsCtrl.validators); //obtiene los errores que estan activos en la sección
                if (resErrs.length)
                    errors.push(resErrs[0].errorMsj);
            });
            return errors;
        }

        if (fields.length) {
            fields.forEach((field) => {
                if (field.forms) {//pregunta si es seccion
                    let form = formGroup.controls[field.formGroupName] as FormGroup; //otiene el formulario correspondiente a la sección
                    let lstErrs = getAllErrors(form, field.forms);
                    if (lstErrs.length)
                        resErrors.push({
                            section: field.nameSection,
                            errores: lstErrs
                        });
                }
            });
        } else {
            resErrors = getAllErrors(formGroup, fields.forms);
        }
        return resErrors;
    }

    /**
     * @description Función que permite tocar al elemento y recuperar los errores del controlador
     * @author carlos.cevallos
     * @date 2023-01-19
     * @param {any} control:any
     * @param {any} options:any
     * @param {any} callback
     * @returns {any}
     */
    static getErrorsTouched(control: any, options: any, callback) {
        control['_markAsTouched'] = control.markAsTouched;
        control.markAsTouched = () => {
            control['_markAsTouched']();
            let errors = this.getErrors(control, options.validators);
            callback(errors);
        }
    }

    /**
     * @description Función que permite asignarle al controlador el estado touched y dirty
     * @author carlos.cevallos
     * @date 2023-01-19
     * @param {any} control:any
     * @returns {any}
     */
    static setTouchedAndDirty(control: any) {
        control?.markAsTouched();
        control?.markAsDirty();
    }

    /**
     * Función que permite (Touch) asignarle al controlador el estado touched y dirty  a todos los controles del formgroup
     * @author Fabián Santander
     * @date 2023-01-25
     * @param {any} formGroup:any
     * @returns {any}
     */
    static touchAllFormFields(formGroup: any) {
        // This code also works in IE 11
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                this.setTouchedAndDirty(control);
            } else if (control instanceof FormGroup) {
                this.touchAllFormFields(control);
            } else if (control instanceof FormArray) {
                this.touchAllFormFields(control);
            }
        });
    }
}