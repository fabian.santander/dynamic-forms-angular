import { FormGroup } from "@angular/forms";
import { EnumFieldAction } from "../enums";
import { ExecuteAction } from "./";

export class ControlFunctions {

    static getChildren(options: any, fg: FormGroup): Promise<any[]> {
        let childrenCtrl: any[] = [];
        if (options.childrenActions) {
            options.childrenActions.forEach(depen => {
                let childCtrlName = depen.childControl;
                let obj = {
                    controlName: childCtrlName,
                    actions: depen.actions,
                    control: fg.get(childCtrlName)
                }
                childrenCtrl.push(obj);
            });
        }
        return Promise.resolve(childrenCtrl);
    }

    static actionChecked(childrenControls, checked) {
        if (childrenControls.length) {
            childrenControls.forEach(child => {
                let actionChecked = child.actions.find(action => action.parentAction == EnumFieldAction.activate);
                if (actionChecked)
                    ExecuteAction(actionChecked, checked, child);
            });
        }
    }
}