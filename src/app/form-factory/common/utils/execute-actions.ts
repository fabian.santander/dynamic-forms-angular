import { EnumFieldAction } from "../enums";
import { ControlActions } from "./control-actions";

export let ExecuteAction = (action: any, value, ctrl: any) => {
  ctrl.action = action;
  switch (action.childAction) {
    case EnumFieldAction.visible:
      ControlActions.showHide(value, ctrl);
      break
    case EnumFieldAction.enabled:
      ControlActions.showHide(value, ctrl);
      break;
  }
}