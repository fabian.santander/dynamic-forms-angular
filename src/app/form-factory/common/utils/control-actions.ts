export class ControlActions {

    static showHide(action: boolean, ctrl: any) {
        let element = document.getElementById(ctrl.controlName) as HTMLElement;
        if (action) {
            ctrl.control.enable();
            element.setAttribute('style', 'display:flex !important');
        } else {
            ctrl.control.disable();
            element.setAttribute('style', 'display:none !important');            
            if(ctrl.action?.reset){
                ctrl.control.reset();
            }
        }
    }

}