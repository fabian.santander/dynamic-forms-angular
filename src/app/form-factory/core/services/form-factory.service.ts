import { CheckboxComponent } from './../../components/form-fields/checkbox/checkbox.component';
import { FormFactoryModel } from '../models/form-factory';
import { InputComponent } from '../../components/form-fields/input/input.component';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { Inject, Injectable } from '@angular/core';
import { TextareaComponent } from '../../components/form-fields/textarea/textarea.component';
import { FieldTypeModel } from '../models/field-type';
import { DropdownComponent } from '../../components/form-fields/dropdown/dropdown.component';
import { EnumTipoValidacion } from '../../common/enums/EnumTipoValidacion';
import { EnumTipoControl } from '../../common/enums/EnumTipoControl';
import { SectionFactoryModel } from '../models/section-factory';


@Injectable({
  providedIn: 'root',
})
export class FormFactoryService {
  /**
   * List of default supported form fields
   */
  private formFields: FieldTypeModel[] = [
    {
      type: EnumTipoControl.input,
      component: InputComponent,
    },
    {
      type: EnumTipoControl.textarea,
      component: TextareaComponent,
    },
    {
      type:  EnumTipoControl.dropdown,
      component: DropdownComponent,
    },
    {
      type:  EnumTipoControl.checkbox,
      component: CheckboxComponent,
    }/*,
    {
      type:  EnumTipoControl.section,
      component: SectionComponent
    },*/
  ];



  constructor(
    @Inject('config') private config: { fields: FieldTypeModel[]},
    private fb: FormBuilder
  ) {
    /**
     * Agregar campos pasados a través de la configuración forRoot({})
     */
    this.formFields = this.formFields.concat(config.fields);
  }

  public get fields(): FieldTypeModel[] {
    return this.formFields;
  }

  //Función que va a permitir Crear la Secciones 
  createSection(sections: SectionFactoryModel[]): FormGroup {   
    const form: FormGroup = this.fb.group({});
    sections.forEach((section: any) => {
      form.addControl(section.formGroupName, this.createForm(section.forms));
    });
    return form;
  }

  /**
   *
   * @param fields JSON object with form fields
   * @returns FormGroup
   */
  
  createForm(fields: FormFactoryModel[]): FormGroup {
    const form: FormGroup = this.fb.group({});
    let validators: ValidatorFn[];

    //Función que va a crear controles y grupos
    const createField = (singleField: any) => {
      validators = [];
      let newControl :any;
      if (singleField.options) {
        if (singleField.options.formControlType === 'array') {
          // Create from array
          newControl = new FormArray([]);
        } else {

          // Create form control
          newControl = new FormControl({
            value: singleField.options?.value ?? '',
            disabled: singleField.options?.disabled && true,
          });
          newControl["msjError"] = [];
        }

        // Add validators to control
        if (singleField.options?.validators?.pattern) {
          validators.push(
            Validators.pattern(singleField.options?.validators?.pattern)
          );
        }
        const lstValidatos = singleField.options?.validators;
        if (lstValidatos) {
          
          lstValidatos.forEach((validator: any) => {
            newControl.msjError.push(validator);
            if (validator.type == EnumTipoValidacion.required) {

              validators.push(Validators.required);
              if (singleField?.controlType == EnumTipoControl.checkbox)
                validators.push(Validators.requiredTrue);
            }
            if (validator.type == EnumTipoValidacion.maxLength) {
              validators.push(
                Validators.maxLength(validator.value)
              );
            }
            if (validator.type == EnumTipoValidacion.minLength) {
              validators.push(Validators.minLength(validator.value));
            }

            if (validator.type == EnumTipoValidacion.min) {
              validators.push(Validators.min(validator.value));
            }
            
            if (validator.type == EnumTipoValidacion.email) {
              validators.push(Validators.email);
            }
          });
        }
        // Set validators
        newControl.setValidators(validators);
        // Add control to form
        form.addControl(singleField.options.formControlName, newControl);
      }
    };

    fields.forEach((field: any) => {
      if (field.dummyFields) {
        field.dummyFields.forEach((dummyField: any) => {
          createField(dummyField);
        });
      }
      // Compruebe si este es un grupo de campos
      if (field.group) {
        // Bucle grupo de campos
        field.group.forEach((singleField: any) => {
          createField(singleField);
        });
      } else {
        createField(field);
      }
    });

    return form;
  }

  getFormGroup(form: FormGroup, field: string): FormGroup {
    return form.get(field) as FormGroup;
  }
}
