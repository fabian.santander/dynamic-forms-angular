import { FieldAction } from "./field-actions";
import { ValidatorField } from "./validator-field";

export interface FormFactoryModel {
  group?: any[];
  controlType?: string;
  colSize?: string; // Grid from primeng example: col-12
  dummyFields?: {
    options: {
      formControlName: string;
      formControlType?: 'array'; // Default is 'control'
      value?: any;
    };
  }[];
  options?: {
    // ? Universal options
    label?: string; // Label above field
    containerClass?: string; // This class is for input field container
    placeholder?: string; // Field placeholder
    formControlName?: string; // Form control name
    value?: any; // Default value for field
    disabled?: boolean; // Define is form field disabled
    validators?: ValidatorField [];
    errorMessage?: string;
    class?: string;
    // ? Input
    type?: 'text';
    childrenActions?:[
      {
        childControl: string,
        actions: FieldAction[]
      }
    ];
    // ? Dropdown
    dropdownOptions?: any;
    optionValue?: string;
    optionLabel?: string;
    id?: number;
    rows?: number | string;
    fieldFormGroup?: string;
  };
}
