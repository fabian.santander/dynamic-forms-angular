import { FormFactoryModel } from "./form-factory";

export interface SectionFactoryModel {
    nameSection: string;
    controlType: string,
    leftIcon?: string;
    rightIcon?: string;
    formGroupName?: string;
    sourceDataSave: string;
    colSize?:string;
    forms: FormFactoryModel[];
}