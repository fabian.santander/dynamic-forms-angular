export interface FieldAction {
    type?: string, //tipo de accion que va a realizar
    parentAction: string,
    childAction: string,  
    reset?: boolean
}