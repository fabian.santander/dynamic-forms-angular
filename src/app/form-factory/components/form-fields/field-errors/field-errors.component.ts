import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-field-errors',
  template: `
  <ng-container *ngIf="errors.length" >
    <ng-container *ngFor="let error of errors">
      <small class="p-error" style="font-size: 12px;" >
       {{error.errorMsj}}
      </small><br>
    </ng-container>
  </ng-container>
  `
})
export class FieldErrorsComponent {

  /**
   * @description
   * Extracted logic for showing form control errors.
   * This component is used inside form factory.
   */

  /**
   * * Inputs
   * @errors Provide all errors from reactive forms
   */
  @Input() errors: any;



}
