import { Component, Input, Output, EventEmitter, OnInit, SkipSelf } from '@angular/core';
import { AbstractControl, ControlContainer, FormGroup } from '@angular/forms';
import { FormFunctions } from 'src/app/form-factory/common/utils/form-functions';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: (container: ControlContainer) => container,    
      deps: [[new SkipSelf(), ControlContainer]],//Permite identificar de manera automatica al formGroup padre  
    },
  ],
})
export class DropdownComponent implements OnInit{
  /**
   * * General
   * @errors Form control errors
   */
  errors: any = {};
  /**
   * * Inputs
   * @options Configuration for this form field
   * @customFormGroup Used when you want to use this component without form-builder
   */
  @Input() options: any;
  @Input() customFormGroup: any;
  /**
   * * Outputs
   * @handleChange Event when item in dropdown is selected
   */
  @Output() handleChange = new EventEmitter<any>();

  constructor(public controlContainer: ControlContainer) {}

  //FromGroup 
  formGroup: FormGroup = this.controlContainer.control as FormGroup;

  //Control del elemento
  control!: AbstractControl;

  ngOnInit(): void {
    this.control = this.formGroup.controls[this.options.formControlName];
    FormFunctions.getErrorsTouched(this.control, this.options, (res)=>{
      this.errors = res;
    });
  }

  onChangeValue(value: any) {
    FormFunctions.setTouchedAndDirty(this.control);
    this.handleChange.emit(value);
  }
}
