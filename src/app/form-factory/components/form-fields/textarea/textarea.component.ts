import { Component, Input, OnInit, SkipSelf } from '@angular/core';
import {
  ControlContainer,
  FormGroupDirective,
  FormGroup,
  AbstractControl,
} from '@angular/forms';
import { FormFunctions } from 'src/app/form-factory/common/utils/form-functions';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: (container: ControlContainer) => container,    
      deps: [[new SkipSelf(), ControlContainer]],//Permite identificar de manera automatica al formGroup padre  
    },
  ],
})
export class TextareaComponent implements OnInit{
  /**
   * * General
   * @errors Form control errors
   */
  errors: any = {};
  /**
   * * Inputs
   * @options Configuration for this form field
   */
  @Input() options: any;

  constructor(public controlContainer: ControlContainer) {}

  /* ====================================
  *                HELPERS
  ======================================= */

  //FromGroup 
  formGroup: FormGroup = this.controlContainer.control as FormGroup;

  //Control del elemento
  control!: AbstractControl;

  ngOnInit(): void {
    this.control = this.formGroup.controls[this.options.formControlName];
    FormFunctions.getErrorsTouched(this.control, this.options, (res)=>{
      this.errors = res;
    });
  }

  onChange(event: Event) {
    FormFunctions.setTouchedAndDirty(this.control);
  }
}
