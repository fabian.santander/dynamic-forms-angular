import { Component, Input, OnInit, SkipSelf } from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  FormGroup
} from '@angular/forms';
import { FormFunctions } from 'src/app/form-factory/common/utils';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: (container: ControlContainer) => container,
      deps: [[new SkipSelf(), ControlContainer]], //Permite identificar de manera automatica al formGroup padre    
    },
  ],
})
export class InputComponent implements OnInit  {

  constructor(public controlContainer: ControlContainer) { }

  /**
   * * General
   * @errors Form control errors
   */
  errors: any = {};
  /**
   * * Inputs
   * @options Configuration for this form field
   * @customFormGroup Used when you want to use this component without form-builder
   */
  @Input() options: any;
  @Input() customFormGroup: any;

  //FromGroup 
  formGroup: FormGroup = this.controlContainer.control as FormGroup;

  //Control del elemento
  control!: AbstractControl;

  ngOnInit(): void {
    this.control = this.formGroup.controls[this.options.formControlName];
    FormFunctions.getErrorsTouched(this.control, this.options, (res) => {
      this.errors = res;
    });
  }

  onChange(event: Event) {
    FormFunctions.setTouchedAndDirty(this.control);
  }
}
