import { Component, Input, OnInit, SkipSelf } from '@angular/core';
import {
  ControlContainer,
  FormGroupDirective,
  FormGroup,
  AbstractControl,
} from '@angular/forms';
import { ControlFunctions, FormFunctions } from 'src/app/form-factory/common/utils';


@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  viewProviders: [
    {
      provide: ControlContainer,
      useFactory: (container: ControlContainer) => container,    
      deps: [[new SkipSelf(), ControlContainer]],//Permite identificar de manera automatica al formGroup padre  
    },
  ],
})
export class CheckboxComponent implements OnInit{
  /**
   * * General
   * @errors Form control errors
   */
  errors: any = {};
  /**
   * * Inputs
   * @options Configuration for this form field
   * @customFormGroup Used when you want to use this component without form-builder
   */
  @Input() options: any;
  @Input() customFormGroup: any;

  constructor(public controlContainer: ControlContainer) { }

  formGroup: FormGroup = this.controlContainer.control as FormGroup;

  //Control del elemento
  control!: AbstractControl;
  childrenControls!: any[];  

  ngOnInit(): void {
    this.control = this.formGroup.controls[this.options.formControlName];
    ControlFunctions.getChildren(this.options, this.formGroup).then((resp) => {
      this.childrenControls = resp;
      ControlFunctions.actionChecked(this.childrenControls, this.options.value);
    });

    FormFunctions.getErrorsTouched(this.control, this.options, (res) => {
      this.errors = res;
    });

    this.control.valueChanges.subscribe(x => {
      FormFunctions.setTouchedAndDirty(this.control);
      ControlFunctions.actionChecked(this.childrenControls, x);
    });
  }

  onChange(event: any) {
    //FormFunctions.setTouchedAndDirty(this.control);
    //ControlFunctions.actionChecked(this.childrenControls, event.checked);
  }

}
