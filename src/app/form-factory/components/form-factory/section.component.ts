import { Component, Input, OnInit } from '@angular/core';
import {
  FormGroup,
} from '@angular/forms';
import { ControlFunctions, FormFunctions } from 'src/app/form-factory/common/utils';


@Component({
  selector: 'app-section-factory',
  templateUrl: './section.component.html'
})
export class SectionComponent implements OnInit{

  /**
   * * Inputs
   * @form Formulario raiz que contiene los formularios de cada sección* 
   * @sections Objeto que contiene todas las secciones
   */
  @Input() form!: FormGroup;
  @Input() sections: any;
  @Input() styleClass!: string;
  activeIndex: number = 0;

  backValuesForm: any;

  ngOnInit(): void {
    this.backValuesForm = this.form.getRawValue();
  }

  getFormbyName(name: string): FormGroup {
    return this.form.controls[name] as FormGroup;
  }

  openPrev() {
    this.activeIndex--;
  }

  openNext() {
    this.activeIndex++;
  }

  onClickSave(section: any) {
    let form = this.getFormbyName(section.formGroupName);
    FormFunctions.touchAllFormFields(form);
    if (form.valid) {
      alert("Es valido");
    }
    FormFunctions.getErrorsFromGroup(form, section);
  }

  onClickCancel(formName: string) {
    let form = this.getFormbyName(formName);
    form.reset();
    form.patchValue(this.backValuesForm[formName]);
  }

}
